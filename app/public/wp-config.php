<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * Localized language
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


/* Add any custom values between this line and the "stop editing" line. */



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
if ( ! defined( 'WP_DEBUG' ) ) {
	define( 'WP_DEBUG', false );
}


define('AUTH_KEY',         'RdAOw5zEUNOfReKoPixBhDUKbhP9unYooFYImTu2ZbX4UeQwML2pbuq3ppEnsxiJL7WoiNDZeHDqyl4TUtjZrw==');
define('SECURE_AUTH_KEY',  'eP5OPfS2hP8ORUVc6QfhLZNmNY4zTNffZcJoJBuo2ggBK/D/Pm6ZJItggn2k7PGLpIHgbo3odP+YqQrNhjBomw==');
define('LOGGED_IN_KEY',    'GqZmP17HLVszB9VUlZjDYcs8LFGV/pXrvE43jtIsEiptKIiE09LWEZJYy09r4xb/QrZrEc+nlcAw4eBylDA+cw==');
define('NONCE_KEY',        '9z749I3ECpHOlk5q/v/Fb6Ut7oVWNLwi2MzEO/rsaPHra8ucedF5O8KuEuvchsvNFzsyardyTYE2VxBlJLkEOw==');
define('AUTH_SALT',        'N9SxCKGZQmjDU3l15n2TLhv9IwEbjgiOWITzFdP29V+CyJDg+u/ipKua2GQIobcHSe9TBKr0iRIjQs4sEa4MhA==');
define('SECURE_AUTH_SALT', '+2O/r+Mqv4uwsVzA/xYWHL7lMrB+8lpZXpeoqCK025YgVfcKaVAfMSGFNHsHCRxhu8b9ARSsDQLDcRSrimlWww==');
define('LOGGED_IN_SALT',   'wix6fkQNZDvBmVty3nJ4QTgDl6JTSuswaFwzFwAUM9LxOrzds17sak1FJFwEZz5oxYj8Qzd1pgU3hCVMwNdOYA==');
define('NONCE_SALT',       '0TJndeK8afkqahRCp3mgEuytdEEBkh608GOKStwLnMqFR57rProO1WqbuWagduXFAMd4wpRL5uqAcl1r/9xBEQ==');
define( 'WP_ENVIRONMENT_TYPE', 'local' );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
